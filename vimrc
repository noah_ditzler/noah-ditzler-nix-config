set nocompatible
set backspace=indent,eol,start
set expandtab tabstop=2 shiftwidth=2 ai
set hidden
set nohlsearch
set termguicolors
if has('nvim')
  autocmd BufWinEnter,WinEnter,TermOpen term://* startinsert
  tnoremap <c-w> <c-\><c-n><c-w>
  com Sterm split | term
  com Vterm vsplit | term
  com Python term python3
  com Spython split | term python3
  com Vpython vsplit | term python3
  lua require 'colorizer'.setup()
  lua require("mason").setup()
endif

